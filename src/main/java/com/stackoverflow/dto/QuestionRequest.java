package com.stackoverflow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionRequest {
    private Long questionId;
    private String tagName;
    private String questionName;
    private String url;
    private String description;
}
