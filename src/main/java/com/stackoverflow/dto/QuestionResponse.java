package com.stackoverflow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionResponse {
    private Long id;
    private String questionName;
    private String url;
    private String description;
    private String userName;
    private String tagName;
    private Integer voteCount;
    private Integer answerCount;
    private String duration;
    private boolean upVote;
    private boolean downVote;
}
