package com.stackoverflow.controller;

import com.stackoverflow.dto.QuestionRequest;
import com.stackoverflow.dto.QuestionResponse;
import com.stackoverflow.service.QuestionService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/api/questions/")
@AllArgsConstructor
public class QuestionController {

    private final QuestionService questionService;

    @PostMapping
    public ResponseEntity<Void> createQuestion(@RequestBody QuestionRequest questionRequest) {
        questionService.save(questionRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<QuestionResponse>> getAllQuestions() {
        return status(HttpStatus.OK).body(questionService.getAllQuestions());
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuestionResponse> getQuestion(@PathVariable Long id) {
        return status(HttpStatus.OK).body(questionService.getQuestion(id));
    }

    @GetMapping("by-tag/{id}")
    public ResponseEntity<List<QuestionResponse>> getQuestionsByTag(Long id) {
        return status(HttpStatus.OK).body(questionService.getQuestionsByTag(id));
    }

    @GetMapping("by-user/{name}")
    public ResponseEntity<List<QuestionResponse>> getQuestionsByUsername(@PathVariable String name) {
        return status(HttpStatus.OK).body(questionService.getQuestionsByUsername(name));
    }
}
