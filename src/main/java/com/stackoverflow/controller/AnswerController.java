package com.stackoverflow.controller;

import com.stackoverflow.dto.AnswerDto;
import com.stackoverflow.service.AnswerService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/answers/")
@AllArgsConstructor
public class AnswerController {
    private final AnswerService answerService;

    @PostMapping
    public ResponseEntity<Void> createAnswer(@RequestBody AnswerDto answerDto) {
        answerService.save(answerDto);
        return new ResponseEntity<>(CREATED);
    }

    @GetMapping("/by-question/{questionId}")
    public ResponseEntity<List<AnswerDto>> getAllAnswerForQuestion(@PathVariable Long questionId) {
        return ResponseEntity.status(OK)
                .body(answerService.getAllAnswersForQuestion(questionId));
    }

    @GetMapping("/by-user/{userName}")
    public ResponseEntity<List<AnswerDto>> getAllAnswerForUser(@PathVariable String userName){
        return ResponseEntity.status(OK)
                .body(answerService.getAllAnswersForUser(userName));
    }

}
