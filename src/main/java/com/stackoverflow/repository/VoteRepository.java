package com.stackoverflow.repository;

import com.stackoverflow.model.Question;
import com.stackoverflow.model.User;
import com.stackoverflow.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findTopByQuestionAndUserOrderByVoteIdDesc(Question question, User currentUser);
}
