package com.stackoverflow.service;

import com.stackoverflow.dto.AnswerDto;
import com.stackoverflow.exceptions.QuestionNotFoundException;
import com.stackoverflow.mapper.AnswerMapper;
import com.stackoverflow.model.Answer;
import com.stackoverflow.model.NotificationEmail;
import com.stackoverflow.model.Question;
import com.stackoverflow.model.User;
import com.stackoverflow.repository.AnswerRepository;
import com.stackoverflow.repository.QuestionRepository;
import com.stackoverflow.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class AnswerService {
    private static final String QUESTION_URL = "";
    private final QuestionRepository questionRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final AnswerMapper answerMapper;
    private final AnswerRepository answerRepository;
    private final MailContentBuilder mailContentBuilder;
    private final MailService mailService;


    public void save(AnswerDto answerDto) {
        Question question = questionRepository.findById(answerDto.getQuestionId())
                .orElseThrow(() -> new QuestionNotFoundException(answerDto.getQuestionId().toString()));
        Answer answer = answerMapper.map(answerDto, question, authService.getCurrentUser());
        answerRepository.save(answer);

        String message = mailContentBuilder.build(authService.getCurrentUser() + " posted a answer on your question." + QUESTION_URL);
        sendAnswersNotification(message, question.getUser());
    }

    private void sendAnswersNotification(String message, User user) {
        mailService.sendMail(new NotificationEmail(user.getUsername() + " Answered on your Question", user.getEmail(), message));
    }

    public List<AnswerDto> getAllAnswersForQuestion(Long questionId) {
        Question question = questionRepository.findById(questionId).orElseThrow(() -> new QuestionNotFoundException(questionId.toString()));
        return answerRepository.findByQuestion(question)
                .stream()
                .map(answerMapper::mapToDto).collect(toList());
    }

    public List<AnswerDto> getAllAnswersForUser(String userName) {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new UsernameNotFoundException(userName));
        return answerRepository.findAllByUser(user)
                .stream()
                .map(answerMapper::mapToDto)
                .collect(toList());
    }


}
