package com.stackoverflow.service;

import com.stackoverflow.dto.VoteDto;
import com.stackoverflow.exceptions.QuestionNotFoundException;
import com.stackoverflow.exceptions.SpringRedditException;
import com.stackoverflow.model.Question;
import com.stackoverflow.model.Vote;
import com.stackoverflow.repository.QuestionRepository;
import com.stackoverflow.repository.VoteRepository;
import com.stackoverflow.model.VoteType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@AllArgsConstructor
public class VoteService {

    private final VoteRepository voteRepository;
    private final QuestionRepository questionRepository;
    private final AuthService authService;

    @Transactional
    public void vote(VoteDto voteDto) {
        Question question = questionRepository.findById(voteDto.getQuestionId())
                .orElseThrow(() -> new QuestionNotFoundException("Question Not Found with ID - " + voteDto.getQuestionId()));
        Optional<Vote> voteByPostAndUser = voteRepository.findTopByQuestionAndUserOrderByVoteIdDesc(question, authService.getCurrentUser());
        if (voteByPostAndUser.isPresent() &&
                voteByPostAndUser.get().getVoteType()
                        .equals(voteDto.getVoteType())) {
            throw new SpringRedditException("You have already "
                    + voteDto.getVoteType() + "'d for this question");
        }
        if (VoteType.UPVOTE.equals(voteDto.getVoteType())) {
            question.setVoteCount(question.getVoteCount() + 1);
        } else {
            question.setVoteCount(question.getVoteCount() - 1);
        }
        voteRepository.save(mapToVote(voteDto, question));
        questionRepository.save(question);
    }

    private Vote mapToVote(VoteDto voteDto, Question question) {
        return Vote.builder()
                .voteType(voteDto.getVoteType())
                .question(question)
                .user(authService.getCurrentUser())
                .build();
    }
}
