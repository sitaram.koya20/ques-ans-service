package com.stackoverflow.service;

import com.stackoverflow.dto.QuestionRequest;
import com.stackoverflow.dto.QuestionResponse;
import com.stackoverflow.exceptions.QuestionNotFoundException;
import com.stackoverflow.exceptions.TagNotFoundException;
import com.stackoverflow.mapper.QuestionMapper;
import com.stackoverflow.model.Question;
import com.stackoverflow.model.Tag;
import com.stackoverflow.model.User;
import com.stackoverflow.repository.QuestionRepository;
import com.stackoverflow.repository.TagRepository;
import com.stackoverflow.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class QuestionService {

    private final QuestionRepository questionRepository;
    private final TagRepository tagRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final QuestionMapper questionMapper;

    public void save(QuestionRequest questionRequest) {
        Tag tag = tagRepository.findByName(questionRequest.getTagName())
                .orElseThrow(() -> new TagNotFoundException(questionRequest.getTagName()));
        questionRepository.save(questionMapper.map(questionRequest, tag, authService.getCurrentUser()));
    }

    @Transactional(readOnly = true)
    public QuestionResponse getQuestion(Long id) {
        Question question = questionRepository.findById(id)
                .orElseThrow(() -> new QuestionNotFoundException(id.toString()));
        return questionMapper.mapToDto(question);
    }

    @Transactional(readOnly = true)
    public List<QuestionResponse> getAllQuestions() {
        return questionRepository.findAll()
                .stream()
                .map(questionMapper::mapToDto)
                .collect(toList());
    }

    @Transactional(readOnly = true)
    public List<QuestionResponse> getQuestionsByTag(Long tagId) {
        Tag tag = tagRepository.findById(tagId)
                .orElseThrow(() -> new TagNotFoundException(tagId.toString()));
        List<Question> questions = questionRepository.findAllByTag(tag);
        return questions.stream().map(questionMapper::mapToDto).collect(toList());
    }

    @Transactional(readOnly = true)
    public List<QuestionResponse> getQuestionsByUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return questionRepository.findByUser(user)
                .stream()
                .map(questionMapper::mapToDto)
                .collect(toList());
    }
}
