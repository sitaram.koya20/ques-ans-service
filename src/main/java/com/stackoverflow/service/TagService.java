package com.stackoverflow.service;

import com.stackoverflow.dto.TagDto;
import com.stackoverflow.exceptions.SpringRedditException;
import com.stackoverflow.mapper.TagMapper;
import com.stackoverflow.model.Tag;
import com.stackoverflow.repository.TagRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class TagService {

    private final TagRepository tagRepository;
    private final TagMapper tagMapper;

    @Transactional
    public TagDto save(TagDto tagDto) {
        Tag save = tagRepository.save(tagMapper.mapDtoToTag(tagDto));
        tagDto.setId(save.getId());
        return tagDto;
    }

    @Transactional(readOnly = true)
    public List<TagDto> getAll() {
        return tagRepository.findAll()
                .stream()
                .map(tagMapper::mapTagToDto)
                .collect(toList());
    }

    public TagDto getTag(Long id) {
        Tag tag = tagRepository.findById(id)
                .orElseThrow(() -> new SpringRedditException("No tag found with ID - " + id));
        return tagMapper.mapTagToDto(tag);
    }
}
