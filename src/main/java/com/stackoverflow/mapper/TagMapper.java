package com.stackoverflow.mapper;

import com.stackoverflow.dto.TagDto;
import com.stackoverflow.model.Question;
import com.stackoverflow.model.Tag;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TagMapper {

    @Mapping(target = "numberOfQuestions", expression = "java(mapQuestions(tag.getQuestions()))")
    TagDto mapTagToDto(Tag tag);

    default Integer mapQuestions(List<Question> numberOfQuestions) {
        return numberOfQuestions.size();
    }

    @InheritInverseConfiguration
    @Mapping(target = "questions", ignore = true)
    Tag mapDtoToTag(TagDto tagDto);
}
