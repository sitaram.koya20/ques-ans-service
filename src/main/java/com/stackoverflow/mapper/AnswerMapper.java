package com.stackoverflow.mapper;

import com.stackoverflow.dto.AnswerDto;
import com.stackoverflow.model.Answer;
import com.stackoverflow.model.Question;
import com.stackoverflow.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AnswerMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "text", source = "answerDto.text")
    @Mapping(target = "createdDate", expression = "java(java.time.Instant.now())")
    @Mapping(target = "question", source = "question")
    @Mapping(target = "user", source = "user")
    Answer map(AnswerDto answerDto, Question question, User user);

    @Mapping(target = "questionId", expression = "java(answer.getQuestion().getQuestionId())")
    @Mapping(target = "userName", expression = "java(answer.getUser().getUsername())")
    AnswerDto mapToDto(Answer answer);
}